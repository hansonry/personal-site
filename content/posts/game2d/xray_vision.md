---
title: "XRay Vision using SDL"
date: 2019-11-21T12:58:40-06:00
draft: false
tags: ["gamedev", "code", "sdl"]
---

As you may know, I am working on an isometric game 
(https://gitlab.com/hansonry/opengetgold).

In this game it is possible for your player to go behind level features. 
So I wanted there to be a way to view the player and its surroundings behind
level features.

One of the things I needed to do in order to accomplish this was to generate an
effect that makes it look like the character is showing though from behind level
features.

If I just drew the character last (on top). It would look like a floating 
character interacting with things that are not visible. So I decided I wanted
to mask out the area around the character and draw that last (on top).

You can do this in SDL, but it was a little tricky to figure out. I needed 
to render to intermediate textures. And I also needed to figure out how
to render only things that were behind the player. I have written about
the algorithm I used to figure that out in my post 
[Isometric XRay Culling]({{< ref "xray_cull.md" >}}).

I ended up with something that looks like this:

![Level](/img/game2d/FinalMasked.png)

To start with I created a mask that you can see below. The white part of the 
image is what you want to keep. The transparent is what will turn transparent. 
You could also stain the image green if you used green instead of white. 
I made the mask twice as big as the isometric tiles so that I could show 
some of the surroundings.

![Level](/img/game2d/IsoMask.png)

The next step is to create two textures to render to. Both are the same size 
as the mask texture. I am calling one `text_player_window`. This will have the 
player and everything "behind" it rendered to it. This will eventually be 
multiplied (or modulated) on top of the mask. That is why its 
[BlendMode](https://wiki.libsdl.org/SDL_BlendMode) set to 
`SDL_BLENDMODE_MOD` I will call the other `text_player_window_masked`. 
This will have the mask rendered to it first and then it will have the 
`text_player_window` rendered to it using multiplication or (or modulated).


The code below can be run once at initialization and shows how to create
the texture targets.


{{< highlight C >}}
ir.text_player_window = SDL_CreateTexture(rend, SDL_PIXELFORMAT_RGBA8888, 
                                                SDL_TEXTUREACCESS_TARGET,
                                                PLAYER_WINDOW_WIDTH,
                                                PLAYER_WINDOW_HEIGHT);
SDL_SetTextureBlendMode(ir.text_player_window, SDL_BLENDMODE_MOD);

ir.text_player_window_masked = SDL_CreateTexture(rend, SDL_PIXELFORMAT_RGBA8888, 
                                                       SDL_TEXTUREACCESS_TARGET,
                                                       PLAYER_WINDOW_WIDTH,
                                                       PLAYER_WINDOW_HEIGHT);
SDL_SetTextureBlendMode(ir.text_player_window_masked, SDL_BLENDMODE_BLEND);

{{< / highlight >}}



Now to render the scene you need to set the render target to 
`text_player_window` and render the player and everything behind it.

{{< highlight C >}}
// Calculate where the texture will hang
player_window_x = xray_focus->destination.x - xray_focus->destination.w / 2;
player_window_y = xray_focus->destination.y - xray_focus->destination.h / 2;

// Render XRay Texture
SDL_SetRenderTarget(rend, ir.text_player_window);
SDL_SetRenderDrawColor(rend, 0, 0, 0, 0);
SDL_RenderClear(rend);
isorender_setxraydrawflags(xray_focus);
isorenderer_pass(rend, -player_window_x, -player_window_y);
{{< / highlight >}}

Here is what `text_player_window` looks like:

![Level](/img/game2d/BackgroundUnmasked.png)

You may notice the small transparent blocks. Those were in front of the player. 
I decided to render them that way to give the player some information on 
what is in front of them. 

Now we will switch the render target to `text_player_window_masked`.
Also we have to set the `text_mask` (mask texture) to have a 
[BlendMode](https://wiki.libsdl.org/SDL_BlendMode) of none. This copies over
the color and the alpha value directly.


{{< highlight C >}}
// Render Masked Texture
SDL_SetRenderTarget(rend, ir.text_player_window_masked);
SDL_SetTextureBlendMode(ir.text_mask, SDL_BLENDMODE_NONE);
SDL_RenderCopy(rend, ir.text_mask, NULL, NULL);
SDL_RenderCopy(rend, ir.text_player_window, NULL, NULL);
{{< / highlight >}}

Here is what `text_player_window_masked` looks like:

![Level](/img/game2d/BackgroundMasked.png)


Finally, we set render back to the screen (NULL). We then render the full
scene. Lastly, we put our new `text_player_window_masked` over where 
the player would be.

{{< highlight C >}}
// Render Again but this time put the new masked texture on top
SDL_SetRenderTarget(rend, NULL);

isorender_setdrawtype(e_iredt_normal);
isorenderer_pass(rend,  0, 0);

view.x = player_window_x;
view.y = player_window_y;
view.w = PLAYER_WINDOW_WIDTH;
view.h = PLAYER_WINDOW_HEIGHT;

SDL_RenderCopy(rend, ir.text_player_window_masked, NULL, &view);
{{< / highlight >}}


You have seen the final render before, but here it is just in case:

![Level](/img/game2d/FinalMasked.png)

I hope someone finds this useful.
