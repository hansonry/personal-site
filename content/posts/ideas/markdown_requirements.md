---
title: "Markdown Requirement System Idea"
date: 2019-11-04T19:07:20-06:00
draft: false
tags: ["idea"]
---

This whole idea of using a preprocessor to build a website is pretty nifty.
Hugo is on to something. This novel technology may be a solution for another
problem I run into on occasion.

Managing requirements can become a huge problem, especial in large 
organizations. I have worked in the medical field (8 years ago) and
in the embedded systems for vehicle filed (now) and both followed the same
path.

They started out with word, and that worked ok for a while. After the projects
grew and they needed to start thinking about traceability they started 
shopping. Both companies bought really complex and expensive solutions. 
Both solutions required lots of training, had syncing features, and both
fell down flat performance wise when adding anything more than 40 
requirements and 4 users.

I did like the direction that Doors was taking though.


Anyway, I am thinking about a tool to replace all of that expensive nonsense.
Here are the features I am thinking about:

* Requirements can be formatted with markdown (you can add tables/images/etc..)
* The database and revision tracking is all done with git
* Something like Hugo can be used to generate a "pretty" version of the
  requirements
* Each requirement would have a GUID and an obsolete flag.
* Easy way to link to other requirements (via GUID)
* The ability to structure requirements into a tree.
* A way to attach Rational to each requirement.
  * Rational for Requirements is 
  underrated.
* Maybe some meta-data about required definitions.
* A way to expert the data into xml, yaml, and/or something else for automatic
  verification or other scripts.
* A way to generate a change set between two versions
   * Maybe point it at two checkout version of the same repo and have go
* Other Cool Features:
   * Convert text from markdown into graphvis diagram?

I can't think of anything else right now. I like Hugo's idea of front matter.
Instead of one markdown file holding a single post. Maybe one markdown file
could hold several requirements. Each requirement with it's own front matter.

Here is what I am thinking:

    ---
    guid: "68d33fd0-81a2-4cc3-8274-58350ebca9e9"
    obsolete: false
    ---
    The Plumbus shall do the following things when stroked
    * Set the _Squirm_ flag to true
    * Set the _Wiggle_ flag to false
    
    R: It was found that most people expected stroking the Plumbus should
       stop it from wiggleing and start to squirm.
    R: Engineering has confirmed that damage can occur if both
       wiggleing and squirming happen at the same time.
    ---
    guid: "55d94d4f-d2bb-4504-8449-c2c5a01a380a"
    obsolete: false
    ---
    The Plumbus shall self clean when the _Squirm_ flag is set to true 
    
    See [68d33fd0-81a2-4cc3-8274-58350ebca9e9] for details.
 
    R: Squirming is the best time to start self cleaning operations
       because the dingle bop is at its most retracted state.

    ---
    guid: "175cf1ba-9b04-49a6-ac6a-5dce6395663c"
    obsolete: false
    ---
    Etc, etc, etc...


I think there could be some potential in this. I don't think my formatting 
is final. You can use my idea if you make the tool free and open source.

